import React from 'react';
import { render, fireEvent } from "react-testing-library";
import "jest-dom/extend-expect";
import ChooseIngredients from '../components/ChooseIngredients';

test("ChooseIngredients component should renders", () => {
    const { getByTestId, getByText } = render(
        <ChooseIngredients />
    );

    expect(getByTestId("choose-ingredients-test")).toBeInTheDocument();
})