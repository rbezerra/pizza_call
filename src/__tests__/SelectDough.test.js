import React from 'react';
import { render, fireEvent } from "react-testing-library";
import "jest-dom/extend-expect";
import SelectDough from '../components/SelectDough';

test("SelectDough component receives props and then renders options", () => {

    const { getByTestId, getByText } = render(
        <SelectDough />
    );

    expect(getByTestId("select-dough-test")).toBeInTheDocument();
    expect(getByTestId("select-dough-test")).toContainElement(getByText('Thin'));
    expect(getByTestId("select-dough-test")).toContainElement(getByText('Thick'));

});

test("SelectedDough should change of value when SelectDough ", () => {
    const { getByTestId, getByText } = render(
        <SelectDough />
    );

    fireEvent.change(getByTestId('select-dough-test'), { target: { value: 2 } });

    expect(getByTestId('selected-test')).toContainElement(getByText('Thick'));



})


