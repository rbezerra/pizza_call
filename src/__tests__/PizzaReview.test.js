import React from 'react';
import {render} from "react-testing-library";
import "jest-dom/extend-expect";
import PizzaReview from '../components/PizzaReview';

test("PizzaReview component receives props and then renders options", () => {

    const { getByTestId } = render(
        <PizzaReview />
    );

    expect(getByTestId("pizza-review-test")).toBeInTheDocument();
});