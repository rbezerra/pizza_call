import React from 'react';
import { render, fireEvent } from "react-testing-library";
import "jest-dom/extend-expect";
import ListOrders from '../components/ListOrders';

test("ListOrders component should renders", () => {
    const { getByTestId, getByText } = render(
        <ListOrders />
    );

    expect(getByTestId("list-orders-test")).toBeInTheDocument();
})