import React from 'react';
import { Button, Row } from 'react-bootstrap';
import TotalPrice from './TotalPrice';

export default class PizzaReview extends React.Component {
    constructor(props) {
        super(props);

        this.saveOrder = this.saveOrder.bind(this);

    }

    saveOrder() {
        this.props.orderPizza();
    }

    render() {
        const ingredients = this.props.ingredients.map((ing) => <li key={ing.id}>{ing.name}</li>);
        return (
            <Row data-testid="pizza-review-test" className="well">
                <Row>
                    <label>Dough: {this.props.dough.name || "None selected"}</label>
                </Row>
                <Row>
                    <label>Ingredients: </label>
                    {ingredients.length > 0 ?
                        (<div>
                            <ul>
                                {ingredients}
                            </ul>
                        </div>) : <label>None selected</label>
                    }
                </Row>
                <Row>
                    <TotalPrice totalPrice={this.props.totalPrice} />
                </Row>
                <Row>
                    <Button bsStyle="success" onClick={this.saveOrder} block>Order It</Button>
                </Row>
            </Row>
        );
    }
}