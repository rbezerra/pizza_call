import React from 'react';
import { Redirect } from 'react-router-dom';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import LinkButton from './LinkButton';

export default class SelectDough extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: '',
            selectedDough: ''
        };

        this.handleChange = this.handleChange.bind(this);

    }

    fetchDoughTypes() {
        return [
            {
                id: 1,
                name: 'Thin',
                description: 'Thin and crusty pizza dough'
            },
            {
                id: 2,
                name: 'Thick',
                description: 'Thick and soft pizza dough'
            },
        ]
    }

    handleChange(event) {
        this.setState({ value: event.target.value });

        const selectedDough = this.fetchDoughTypes().filter((dough) => dough.id.toString() === event.target.value);
        this.props.changeSelectedDough(selectedDough[0]);


    }

    redirectChooseIngredients() {
        return <Redirect to="choose-ingredients" />
    }

    render() {

        const options = this.fetchDoughTypes().map((dough) => <option value={dough.id} key={dough.id} description={dough.description}>{dough.name}</option>)
        return (
            <div className="well">

                <FormGroup controlId="formControlsSelect">
                    <ControlLabel>Select your type of dough</ControlLabel>
                    <FormControl
                        componentClass="select"
                        value={this.state.value}
                        placeholder="Select ..."
                        onChange={this.handleChange}
                    >
                        <option value="">Select ...</option>
                        {options}

                    </FormControl>
                </FormGroup>
                <LinkButton to="/choose-ingredients" bsStyle="success" bsSize="large" block>Next >></LinkButton>

            </div>

        );

    }
}