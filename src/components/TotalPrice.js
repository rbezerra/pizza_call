import React from 'react';

export default ({totalPrice}) => (
    <h2><label>Total: {parseFloat(totalPrice).toFixed(2)} €</label></h2>
);