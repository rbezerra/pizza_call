import React from 'react';
import { Table } from 'react-bootstrap';

export default class ListOrders extends React.Component {
    render() {
        const orders = this.props.orders.map((pizza, idx) => {
            const ingredients = pizza.ingredients.map((ingredient, idx) => {
                return <li>{ingredient.name}</li>
            });
            return (
                <tr key={idx}>
                    <td>{idx}</td>
                    <td>{pizza.dough.name}</td>
                    <td><ul>{ingredients}</ul></td>
                    <td>{pizza.totalPrice.toFixed(2)}</td>
                </tr>
            );
        });
        return (
            <div className="well">
                <Table striped hover >
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Dough</th>
                            <th>Ingredients</th>
                            <th>Price (€)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orders}
                    </tbody>
                </Table>
            </div>
        );
    }
}