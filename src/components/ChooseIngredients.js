import React from 'react';
import { Row, Col } from 'react-bootstrap';
import TotalPrice from './TotalPrice';
import LinkButton from './LinkButton';

export default class ChooseIngredients extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            totalPrice: 0,
            selectedIngredients: []
        }

        this.updatePriceAndSelected = this.updatePriceAndSelected.bind(this);
    }

    fetchIngredients() {
        return [
            {
                id: 1,
                name: 'Sliced Pepperoni'
            },
            {
                id: 2,
                name: 'Ham'
            },
            {
                id: 3,
                name: 'Mushroom'
            },
            {
                id: 4,
                name: 'Parmesan Cheese'
            },
            {
                id: 5,
                name: 'Anchovy'
            }
        ]
    }

    updatePriceAndSelected(event) {
        const newIngredient = {
            name: event.target.name,
            id: event.target.value
        };

        let newPrice = null;
        let selectedIngredientsArray = this.state.selectedIngredients || [];

        if (event.target.checked) {
            newPrice = this.state.totalPrice + 0.50;
            selectedIngredientsArray.push(newIngredient);
        } else {
            newPrice = this.state.totalPrice - 0.50;
            selectedIngredientsArray.splice(selectedIngredientsArray.findIndex(i => i.id === newIngredient.id), 1);
        }

        this.setState({
            totalPrice: newPrice,
            selectedIngredients: selectedIngredientsArray
        });

        const newInfo = {
            totalPrice: newPrice,
            selectedIngredients: selectedIngredientsArray
        }

        this.props.changeTotalPrice(newInfo);

    }

    render() {

        const ingredients = this.fetchIngredients().map((ingredient) => <div key={ingredient.id}><input type="checkbox" name={ingredient.name} value={ingredient.id} onChange={this.updatePriceAndSelected} /> {ingredient.name}</div>)
        return (
            <Row data-testid="choose-ingredients-test" className="well">
                <label>Select your additional ingredients:</label>
                {ingredients}
                <TotalPrice totalPrice={this.state.totalPrice} />
                <Row>
                    <Col xs={6} md={6}>
                        <LinkButton to="/choose-dough" bsStyle="success" bsSize="large" block>{"<< Prev"}</LinkButton>
                    </Col>
                    <Col xs={6} md={6}>
                        <LinkButton to="/review-order" bsStyle="success" bsSize="large" block>Next >></LinkButton>
                    </Col>
                </Row>

            </Row>
        );
    }
}