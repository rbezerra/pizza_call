import React from 'react';

export default class SelectedDough extends React.Component {
    render(){
        return <div data-testid="selected-test">
                    <label>Type of Dough: </label> <span>{this.props.dough.name}</span>
                    <label>Description: </label> <span>{this.props.dough.description}</span>
                </div>
    }
}