import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import SelectDough from './components/SelectDough';
import ChooseIngredients from './components/ChooseIngredients';
import PizzaReview from './components/PizzaReview';
import ListOrders from './components/ListOrders';
import LinkButton from './components/LinkButton';
import { Row, Navbar, Grid } from 'react-bootstrap';



class App extends Component {

  constructor() {
    super();
    this.state = {
      selectedDough: '',
      selectedIngredients: [],
      totalPrice: 0,
      pizzasOrdered: []
    };

    this.routes = [
      {
        path: "/",
        exact: true,
        sidebar: () => <div>home!</div>,
        main: () => <div className="well">
          <h2>Welcome to PizzaCall</h2>
          <div><span>There's {this.state.pizzasOrdered.length} pizza(s) ordered</span></div>
          <LinkButton to="/choose-dough" bsStyle="success" block>Order a pizza!!!</LinkButton>
        </div>
      },
      {
        path: "/choose-dough",
        sidebar: () => <div>Choosing dough!</div>,
        main: () => <SelectDough changeSelectedDough={this.selectedDoughCallback} />
      },
      {
        path: "/choose-ingredients",
        sidebar: () => <div>Choosing ingredients!</div>,
        main: () => <ChooseIngredients changeTotalPrice={this.chooseIngredientsCallback} />
      },
      {
        path: "/review-order",
        sidebar: () => <div>Reviewing your order!</div>,
        main: () => <PizzaReview dough={this.state.selectedDough} ingredients={this.state.selectedIngredients} totalPrice={this.state.totalPrice} orderPizza={this.orderPizzaCallback} />
      },
      {
        path: "/previous-orders",
        sidebar: () => <div>List previous orders</div>,
        main: () => <ListOrders orders={this.state.pizzasOrdered} />
      }
    ];
  }



  selectedDoughCallback = (selectedDough) => {
    this.setState({ selectedDough: selectedDough });
  }

  chooseIngredientsCallback = (newInfo) => {
    this.setState({
      totalPrice: newInfo.totalPrice,
      selectedIngredients: newInfo.selectedIngredients
    });
  }

  orderPizzaCallback = () => {

    if(!this.state.selectedDough) return alert('You should select a dough!');
    

    const orderedPizza = {
      dough: this.state.selectedDough,
      ingredients: this.state.selectedIngredients,
      totalPrice: this.state.totalPrice
    };

    let pizzasOrderedArray = this.state.pizzasOrdered;
    pizzasOrderedArray.push(orderedPizza);

    this.setState({ pizzasOrdered: pizzasOrderedArray });

    alert('Thanks!!! Your order will be ready anytime soon!');
  }

  render() {
    return (
      <Router>
        <Grid>
          <Row>
            <Navbar style={{ marginTop: "10px" }}>
              <Navbar.Header>
                <Navbar.Brand>
                  <Link to="/">PizzaCall</Link>
                </Navbar.Brand>
              </Navbar.Header>
            </Navbar>
          </Row>
          <Row>


            <div style={{ display: "flex" }} className="well">
              <div
                style={{
                  padding: "10px",
                  width: "20%"
                }}
              >
                <h2>Menu</h2>
                <ul style={{ listStyleType: "none", padding: 0 }}>
                  <li>
                    <Link to="/">Home</Link>
                  </li>
                  <li>
                    <Link to="/choose-dough">Dough</Link>
                  </li>
                  <li>
                    <Link to="/choose-ingredients">Ingredients</Link>
                  </li>
                  <li>
                    <Link to="/review-order">Review Order</Link>
                  </li>
                  <li>
                    <Link to="/previous-orders">Previous Orders</Link>
                  </li>
                </ul>

                {this.routes.map((route, index) => (
                  <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    component={route.sidebar}
                  />
                ))}
              </div>
              <div style={{ flex: 2, padding: "10px" }}>
                {this.routes.map((route, index) => (
                  <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    component={route.main}
                  />
                ))}
              </div>
            </div>
          </Row>
        </Grid>
      </Router>
    )
  }
}

export default App;
